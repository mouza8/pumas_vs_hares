CC = gcc 
CFLAGS = -O3

SOURCEDIR = src/
INCDIR = inc/
TESTDIR = Testing/
OUTPUTDIR = out/
OBJDIR = obj/

OBJS = main.o updateValues.o computations.o readInput.o arralloc.o output.o error.o getTime.o
EXEC = main

MFLAGS = -w

all: exec tests

exec: $(OBJS) Makefile
	$(CC) $(CFLAGS) -o $(EXEC) $(OBJDIR)*.o

%.c: $(INCDIR)%.h
	
%.o: $(SOURCEDIR)%.c
	$(CC) $(CFLAGS) -c -o $(OBJDIR)$@ $< 

tests:
	cd $(TESTDIR) && $(MAKE) $(MFLAGS)

clean: 
	rm -f $(OBJDIR)*.o $(EXEC)
	cd $(TESTDIR) && $(MAKE) $(MFLAGS) clean

clean_img:
	rm $(OUTPUTDIR)*.ppm

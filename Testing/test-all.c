#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <stdlib.h>
#include <stdio.h>

#include "../inc/computations.h"
#include "../inc/output.h"
#include "../inc/readInput.h"
#include "../inc/updateValues.h"
#include "../inc/error.h"
#include "../inc/arralloc.h"

double m_Tolerance = 1e-10;


/*
 * Tests for function readParameters() in file readInput.c
 */

void test_readParameters_correctness(void)
{
	double param[7], actual_values[7] = {0.08, 0.04, 0.02, 0.06, 0.2, 0.2, 0.4};
	int i, return_val = -1;
	
	for(i=0; i<7; i++)
		param[i] = -1;
	
	return_val = readParameters("input-files/parameters_correct.txt", &param[0], &param[1], &param[2], &param[3], &param[4], &param[5], &param[6]);
	
	CU_ASSERT_EQUAL(return_val, 0);
	for(i=0; i<7; i++)
		CU_ASSERT_DOUBLE_EQUAL(param[i], actual_values[i], m_Tolerance);
}

void test_readParameters_nofile(void)
{
	double param[7];
	int return_val = 0;
	
	return_val = readParameters("non existing", &param[0], &param[1], &param[2], &param[3], &param[4], &param[5], &param[6]);
	
	CU_ASSERT_EQUAL(errorid, ERROR_OPENING_FILE);
	CU_ASSERT(return_val < 0);
}

void test_readParameters_wrongfile(void)
{
	double param[7];
	int return_val = 0;
	
	return_val = readParameters("input-files/parameters_wrong.txt", &param[0], &param[1], &param[2], &param[3], &param[4], &param[5], &param[6]);
	
	CU_ASSERT_EQUAL(errorid, ERROR_INCORRECT_FILE);
	CU_ASSERT(return_val < 0);
}

void test_readParameters_invalidValues(void)
{
	double param[7];
	int return_val = 0;
	
	return_val = readParameters("input-files/parameters_wrong1.txt", &param[0], &param[1], &param[2], &param[3], &param[4], &param[5], &param[6]);
	
	CU_ASSERT_EQUAL(errorid, ERROR_INVALID_VALUES);
	CU_ASSERT(return_val < 0);
}


/*
 * Tests for function readMap() in file readInput.c
 */

void test_readMap_correctness(void)
{
	MapCell **map;
	int mapbits[5][5] = {{0, 0, 0, 0, 0} , {0, 0, 1, 0, 0} , {0, 1, 1, 1, 0} , {0, 1, 1, 0, 0} , {0, 0, 1, 0, 0}};
	int i, j, nx, ny;
	
	map = readMap("input-files/map_for_testing-correct.dat", &nx, &ny);
		
	CU_ASSERT(nx == 7 && ny == 7);
	for(i=1; i<nx-1; i++)
		for(j=1; j<ny-1; j++)
			CU_ASSERT_EQUAL(map[i][j].bit , mapbits[i-1][j-1]);
	
	free(map);
}

void test_readMap_nofile(void)
{
	MapCell **map;
	int nx, ny;
		
	map = readMap("non existing", &nx, &ny);

	CU_ASSERT_EQUAL(errorid, ERROR_OPENING_FILE);
	CU_ASSERT_PTR_NULL(map);
}

void test_readMap_invalidValues(void)
{
	MapCell **map;
	int nx, ny;
		
	map = readMap("input-files/map_for_testing-wrong1.dat", &nx, &ny);
	CU_ASSERT_EQUAL(errorid, ERROR_INVALID_VALUES);
	CU_ASSERT_PTR_NULL(map);
	
	map = readMap("input-files/map_for_testing-wrong2.dat", &nx, &ny);
	CU_ASSERT_EQUAL(errorid, ERROR_INVALID_VALUES);
	CU_ASSERT_PTR_NULL(map);
}


/*
 * Test(s) for function insertHalos() in file computations.c
 */

void test_insertHalos_correctness(void)
{
	MapCell **map;
	int i, j, nx, ny;
	
	map = readMap("input-files/map_for_testing-correct.dat", &nx, &ny);
	
	insertHalos(nx, ny, map);
	
	for(j=0; j<ny; j++)
	{
		CU_ASSERT_EQUAL(map[0][j].bit , 0);
		CU_ASSERT_EQUAL(map[nx-1][j].bit , 0);
	}
	
	for(i=0; i<nx; i++)
	{
		CU_ASSERT_EQUAL(map[i][0].bit , 0);
		CU_ASSERT_EQUAL(map[i][ny-1].bit , 0);
	}
	
	free(map);
}

void test_insertHalos_nullmap(void)
{	
	CU_ASSERT_EQUAL(insertHalos(5, 5, NULL), -1);
	CU_ASSERT_EQUAL(errorid, ERROR_NULL_POINTER);
}


/*
 * Test(s) for function initializeMapDensities() in file computations.c
 */

void test_initializeMapDensities_correctness(void)
{
	MapCell **map;
	int mapbits[5][5] = {{0, 0, 0, 0, 0} , {0, 0, 1, 0, 0} , {0, 1, 1, 1, 0} , {0, 1, 1, 0, 0} , {0, 0, 1, 0, 0}};
	int i, j, nx, ny;
	double hareden[5][5], pumaden[5][5];
	double **oldhareden, **oldpumaden, **newhareden, **newpumaden;
	
	map = readMap("input-files/map_for_testing-correct.dat", &nx, &ny);
	oldhareden = (double**)arralloc(sizeof(double), 2, nx, ny);
	oldpumaden = (double**)arralloc(sizeof(double), 2, nx, ny);
	newhareden = (double**)arralloc(sizeof(double), 2, nx, ny);
	newpumaden = (double**)arralloc(sizeof(double), 2, nx, ny);
	
	srand(5);
	initializeMapDensities(nx, ny, map, oldhareden, oldpumaden, newhareden, newpumaden);
	
	srand(5);
	for(i=0; i<5; i++)
	{
		for(j=0; j<5; j++)
		{
			hareden[i][j] = (double)(rand() % 6) * mapbits[i][j];
			pumaden[i][j] = (double)(rand() % 6) * mapbits[i][j];
		}
	}
		
	for(i=1; i<nx-1; i++)
	{
		for(j=1; j<ny-1; j++)
		{
			CU_ASSERT_EQUAL(newhareden[i][j] , hareden[i-1][j-1]);
			CU_ASSERT_EQUAL(newpumaden[i][j] , pumaden[i-1][j-1]);
			CU_ASSERT_EQUAL(oldhareden[i][j] , hareden[i-1][j-1]);
			CU_ASSERT_EQUAL(oldpumaden[i][j] , pumaden[i-1][j-1]);
		}
	}
	
	free(map);
	free(oldpumaden);
	free(oldhareden);
	free(newpumaden);
	free(newhareden);
}

void test_initializeMapDensities_nullpointer(void)
{	
	CU_ASSERT_EQUAL(initializeMapDensities(5, 5, NULL, NULL, NULL, NULL, NULL), -1);
	CU_ASSERT_EQUAL(errorid, ERROR_NULL_POINTER);
}


/*
 * Test(s) for function calculateLandNeighbours() in file computations.c
 */

void test_calculateLandNeighbours_correctness(void)
{
	MapCell **map;
	int i, j, nx, ny, k;
	int landneigh[7] = {1, 2, 4, 1, 2, 3, 1};
		
	map = readMap("input-files/map_for_testing-correct.dat", &nx, &ny);
	
	calculateLandNeighbours(nx, ny, map);
	
	k=0;
	for(i=1; i<nx-1; i++)
		for(j=1; j<ny-1; j++)
			if (map[i][j].bit == 1)
			{
				CU_ASSERT_EQUAL(map[i][j].numberOfLandNeighbours , landneigh[k]);
				k++;
			}
			
	free(map);
}

void test_calculateLandNeighbours_nullmap(void)
{	
	CU_ASSERT_EQUAL(calculateLandNeighbours(5, 5, NULL), -1);
	CU_ASSERT_EQUAL(errorid, ERROR_NULL_POINTER);
}


/*
 * Test(s) for function calculateLandCells() in file computations.c
 */

void test_calculateLandCells_correctness(void)
{
	MapCell **map;
	int i, j, nx, ny, k;
	int land;
		
	map = readMap("input-files/map_for_testing-correct.dat", &nx, &ny);
	
	land = calculateLandCells(nx, ny, map);
	CU_ASSERT_EQUAL(land, 7);
			
	free(map);
}

void test_calculateLandCells_nullmap(void)
{	
	CU_ASSERT_EQUAL(calculateLandCells(5, 5, NULL), -1);
	CU_ASSERT_EQUAL(errorid, ERROR_NULL_POINTER);
}


/*
 * Test(s) for function getAvges() in file computations.c
 */

void test_getAvges_correctness(void)
{
	MapCell **map;
	int i, j, nx, ny;
	double avgP, avgH;
	double **hareden, **pumaden;
		
	map = readMap("input-files/map_for_testing-correct.dat", &nx, &ny);
	hareden = (double**)arralloc(sizeof(double), 2, nx, ny);
	pumaden = (double**)arralloc(sizeof(double), 2, nx, ny);
	
	for(i=1; i<nx-1; i++)
		for(j=1; j<ny-1; j++)
		{
			pumaden[i][j] = i * map[i][j].bit;
			hareden[i][j] = j * map[i][j].bit;
		}
		
	getAvges(nx, ny, 7, hareden, pumaden, &avgP, &avgH);
	
	CU_ASSERT_DOUBLE_EQUAL(avgP, 3.42857142857143, m_Tolerance);
	CU_ASSERT_DOUBLE_EQUAL(avgH, 2.85714285714286, m_Tolerance);
	
	free(map);
	free(pumaden);
	free(hareden);
}

void test_getAvges_nullpointer(void)
{	
	CU_ASSERT_EQUAL(getAvges(5, 5, 7, NULL, NULL, NULL, NULL), -1);
	CU_ASSERT_EQUAL(errorid, ERROR_NULL_POINTER);
}


/*
 * Test(s) for functions dmin() and dmax() in file computations.c
 */

void test_dmin(void)
{
	CU_ASSERT_DOUBLE_EQUAL(dmin(4.1234, 2.54), 2.54, m_Tolerance);
}

void test_dmax(void)
{
	CU_ASSERT_DOUBLE_EQUAL(dmax(4.1234, 2.54), 4.1234, m_Tolerance);
}


/*
 * Test(s) for function getMaxMins() in file computations.c
 */

void test_getMaxMins_correctness(void)
{
	MapCell **map;
	int i, j, nx, ny;
	double minp, minh, maxp, maxh;
	double **hareden, **pumaden;
		
	map = readMap("input-files/map_for_testing-correct.dat", &nx, &ny);
	hareden = (double**)arralloc(sizeof(double), 2, nx, ny);
	pumaden = (double**)arralloc(sizeof(double), 2, nx, ny);
	
	for(i=1; i<nx-1; i++)
		for(j=1; j<ny-1; j++)
		{
			pumaden[i][j] = i * map[i][j].bit;
			hareden[i][j] = j * map[i][j].bit;
		}
		
	getMaxMins(nx, ny, hareden, pumaden, &minp, &minh, &maxp, &maxh);
	CU_ASSERT_EQUAL(minp, 0);
	CU_ASSERT_EQUAL(minh, 0);
	CU_ASSERT_EQUAL(maxp, 5);
	CU_ASSERT_EQUAL(maxh, 4);
	
	free(map);
	free(pumaden);
	free(hareden);
}

void test_getMaxMins_nullpointer(void)
{	
	CU_ASSERT_EQUAL(getMaxMins(5, 5, NULL, NULL, NULL, NULL, NULL, NULL), -1);
	CU_ASSERT_EQUAL(errorid, ERROR_NULL_POINTER);
}


/*
 * Test(s) for function mapPixel() in file output.c
 */

void test_mapPixel_correctness(void)
{
	CU_ASSERT_EQUAL(mapPixel(0.5, 7.5, 4, 0, 10), 5);
}

void test_mapPixel_wrongInput(void)
{
	CU_ASSERT_EQUAL(mapPixel(2.5, 0.5, 4, 10, 2), -1);
	CU_ASSERT_EQUAL(errorid, ERROR_MAP_PIXEL);
}


/*
 * Test(s) for function calculateNewAnimalDensities() in file updateValues.c
 */

void test_calculateNewAnimalDensities_correctness(void)
{
	double ret1, ret2;
	
	calculateNewAnimalDensities(0.5, 7.5, 4.1, 0.8, 10.2, 0.5, 7.5, 4.1, 0.8, 10.2, 3, 0.5, 7.5, 4.1, 0.8, 10.2, 0.5, 7.5, &ret1, &ret2);
	
	CU_ASSERT_DOUBLE_EQUAL(ret1, 637.1375, m_Tolerance);
	CU_ASSERT_DOUBLE_EQUAL(ret2, 42.875, m_Tolerance);
}

void test_calculateNewAnimalDensities_nullpointer(void)
{
	int ret = 0;
	
	ret = calculateNewAnimalDensities(0.5, 7.5, 4.1, 0.8, 10.2, 0.5, 7.5, 4.1, 0.8, 10.2, 3, 0.5, 7.5, 4.1, 0.8, 10.2, 0.5, 7.5, NULL, NULL);
	
	CU_ASSERT_EQUAL(errorid, ERROR_NULL_POINTER);
	CU_ASSERT_EQUAL(ret, -1);
}


int main()
{
	CU_pSuite readInput_pSuite = NULL;
	CU_pSuite computations_pSuite = NULL;
	CU_pSuite output_pSuite = NULL;
	CU_pSuite update_pSuite = NULL;

	/* initialize the CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry())
		return CU_get_error();

	/* add suites to the registry */
	readInput_pSuite = CU_add_suite("ReadInput Suite", NULL, NULL);
	if (NULL == readInput_pSuite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	computations_pSuite = CU_add_suite("Computations Suite", NULL, NULL);
	if (NULL == computations_pSuite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	output_pSuite = CU_add_suite("Output Suite", NULL, NULL);
	if (NULL == output_pSuite) {
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	update_pSuite = CU_add_suite("Update-values Suite", NULL, NULL);
	if (NULL == update_pSuite) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	/* add the tests to the appropriate suites */
	/* NOTE - ORDER IS IMPORTANT */
	if ( (NULL == CU_add_test(readInput_pSuite, "Testing readParameters correctness", test_readParameters_correctness)) ||
		 (NULL == CU_add_test(readInput_pSuite, "Testing readParameters when input file does not exist", test_readParameters_nofile)) ||
		 (NULL == CU_add_test(readInput_pSuite, "Testing readParameters when input file is not formated correctly", test_readParameters_wrongfile)) ||
		 (NULL == CU_add_test(readInput_pSuite, "Testing readParameters when input file has invalid values", test_readParameters_invalidValues)) ||
		 (NULL == CU_add_test(readInput_pSuite, "Testing readMap correctness", test_readMap_correctness)) ||
		 (NULL == CU_add_test(readInput_pSuite, "Testing readMap when input file does not exist", test_readMap_nofile)) ||
		 (NULL == CU_add_test(readInput_pSuite, "Testing readMap when input file has invalid values", test_readMap_invalidValues)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing insertHalos correctness", test_insertHalos_correctness)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing insertHalos when map is NULL", test_insertHalos_nullmap)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing initializeMapDensities correctness", test_initializeMapDensities_correctness)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing initializeMapDensities when any array is NULL", test_initializeMapDensities_nullpointer)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing calculateLandNeighbours correctness", test_calculateLandNeighbours_correctness)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing calculateLandNeighbours when map is NULL", test_calculateLandNeighbours_nullmap)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing calculateLandCells correctness", test_calculateLandCells_correctness)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing calculateLandCells when map is NULL", test_calculateLandCells_nullmap)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing getAvges correctness", test_getAvges_correctness)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing getAvges when any pointer argument is NULL", test_getAvges_nullpointer)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing dmin", test_dmin)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing dmax", test_dmax)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing getMaxMins correctness", test_getMaxMins_correctness)) ||
		 (NULL == CU_add_test(computations_pSuite, "Testing getMaxMins when any pointer argument is NULL", test_getMaxMins_nullpointer)) ||
		 (NULL == CU_add_test(output_pSuite, "Testing mapPixel correctness", test_mapPixel_correctness)) ||
		 (NULL == CU_add_test(output_pSuite, "Testing mapPixel when its input is wrong", test_mapPixel_wrongInput)) ||
		 (NULL == CU_add_test(update_pSuite, "Testing calculateNewAnimalDensities correctness", test_calculateNewAnimalDensities_correctness)) ||
		 (NULL == CU_add_test(update_pSuite, "Testing calculateNewAnimalDensities when any pointer argument is NULL", test_calculateNewAnimalDensities_nullpointer)) )
	{
		CU_cleanup_registry();
		return CU_get_error();
	}

	/* Run all tests using the CUnit Basic interface */
	CU_basic_set_mode(CU_BRM_VERBOSE); 
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}

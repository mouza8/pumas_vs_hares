#ifndef error_H__
#define error_H__

/* 
 * This file contains the various error values, the global error variable errorid
 * and a function that prints a message according to the error that occured
 */

#define SUCCESS					 0
#define ERROR_OPENING_FILE		-1
#define ERROR_INCORRECT_FILE	-2
#define ERROR_INVALID_VALUES	-3
#define ERROR_NULL_POINTER		-4
#define ERROR_MAP_PIXEL			-5
#define ERROR_WRITE_PPM			-6

int errorid;

/*
 * Prints an error according to the value of the global variable errorid
 */
void printError(const char *);

#endif
#ifndef updateValues_H__
#define updateValues_H__

/*
 * Calculate the new densities of both animals
 */
int calculateNewAnimalDensities(double, double, double, double, double, double, double, double, double, double, int, double, double, double, double, double, double, double, double *, double *);

#endif
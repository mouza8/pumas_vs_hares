#ifndef output_H__
#define output_H__

#define PPMTYPE "P3"
#define MAXCOLOR 255
#define LIGHTGRAY "173 173 173"
#define DARKGRAY "140 140 140"

/*
* Does a linear map from double value to a pixel value.
* The output is clamped to stay inside the defined min and max.
*/
int mapPixel(double, double, double, int, int);

/*
* Writes out a PPM format picture representing the current map.
* 
* land  - black
* water - light gray
* pumas - red (according to density)
* hares - green (according to density) 
*/
int writePPM(int, int, MapCell **, double **, double **, char *);

/*
 * Print user settable parameters
 */
void printParameters(double, double, double, double, double, double, double);

/*
 * Print densities of land cells
 */
void printDensities(int, int, MapCell **, double **, double **);

/*
 * Print map bits (1 for land, 0 for water)
 */
void printMap(int, int, MapCell **);

/*
 * Print the usage message for main
 */
void printUsage(void);

#endif
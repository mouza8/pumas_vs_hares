#ifndef getTime_H__
#define getTime_H__

/*
 * Returns time elapsed between two timevals.
 */
double timeDifference(struct timeval *, struct timeval *); 

#endif

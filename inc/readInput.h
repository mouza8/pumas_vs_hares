#ifndef readInput_H__
#define readInput_H__

#define NMAX 2000
#define MAX_FILENAME 25

/*
 * This struct represents a map cell. For each cell, it stores
 * two integer values named bit and numberOfLandNeighbours.
 */
typedef struct
{
	/*  
	 * bit is equal to 0 for water or 1 for land
	 */
	int bit;

	/*
	 * numberOfLandNeighbours is equal the number of land neighbours of this cell
	 */
	int numberOfLandNeighbours;
	
} MapCell;

/*
 * Reads user's settable parameters
 */
int readParameters(char *, double *, double *, double *, double *, double *, double *, double *);

/*
 * Reads ascii map file
 */
MapCell** readMap(char *, int *, int *);

#endif
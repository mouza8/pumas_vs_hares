#ifndef computations_H__
#define computations_H__

#include "../inc/readInput.h"

/*
 * Insert halos into map
 */
int insertHalos(int, int, MapCell **);

/*
 * Initialize map densities
 */
int initializeMapDensities(int, int, MapCell **, double **, double **, double **, double **);

/*
 * Calculate number of land neighbours for each cell
 */
int calculateLandNeighbours(int, int, MapCell **);

/*
 * Calculates the total land cells in the map
 */
int calculateLandCells(int, int, MapCell **);

/*
* Calculates the average density of pumas and hares.
*/
int getAvges(int, int, int, double **, double **, double *, double *);

/*
* Returns the smaller of the two values.
*/
double dmin(double, double);

/*
* Returns the larger of the two values.
*/
double dmax(double, double);

/*
* Calculates the min and max density of pumas and hares.
*/
int getMaxMins(int, int, double **, double **, double *, double *, double *, double *);

#endif
Programming Skills Group Project

Group 1:

Angus yann walter Maidment s0905852@sms.ed.ac.uk  
Justs Zarins s0930249@sms.ed.ac.uk  
Konstantinos Mouzakitis s1359653@sms.ed.ac.uk  
Georgios Iniatis s1363721@sms.ed.ac.uk  

------------------
Compiling the code
------------------

(1) To compile the code, create object files and executable file execute the following command:

make all

(2) To delete object files and executable execute the following command:

make clean

(3) To delete all ppm output files execute the following command:

make clean_img


----------------
Running the code
----------------

Example: ./main -T 125 -f dat/islands.dat -o

-f path_to_input_map_filename
  (required) Reads the input map file 

-o 
  (optional) Enables periodic outputs. Each output ppm file is   		  stored in the out/ directory

-T output_frequency
  (optional) Sets the output frequency to output_frequency.    		  Default output frequency is set to 125

-h 
  (optional) Prints a help-message to the screen


-------------------------------
Changing algorithm's parameters
-------------------------------

Algorithm parameters take their values from parameters.txt which is located in the dat/ directory. If you want to use different parameter values, just modify the values in that file.
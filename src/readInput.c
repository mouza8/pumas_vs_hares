#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../inc/arralloc.h"
#include "../inc/error.h"
#include "../inc/readInput.h"

/*
 * Reads user's settable parameters
 */
int readParameters(char *fileName, double *r, double *a, double *b, double *m, double *k, double *l, double *dt)
{
	int lineNumber;
	char *tempValue, *temp;
	char line[10];
	FILE *filePointer;
		
	filePointer = fopen(fileName, "r");
	if(!filePointer)
	{
		errorid = ERROR_OPENING_FILE;
		return -1;
	}
	
	lineNumber = 1;
	while(fgets(line, sizeof(line), filePointer) != NULL)
	{
		temp = strtok(line, ":");
		tempValue = strtok(NULL, " ");
		
		switch(lineNumber)
		{
			case 1: 
				*r = atof(tempValue);
				break;
			case 2: 
				*a = atof(tempValue);
				break;
			case 3: 
				*b = atof(tempValue);
				break;
			case 4: 
				*m = atof(tempValue);
				break;
			case 5: 
				*k = atof(tempValue);
				break;
			case 6: 
				*l = atof(tempValue);
				break;
			case 7: 
				*dt = atof(tempValue);
				break;
		}
		
		lineNumber++;
	}
	
	fclose(filePointer);
	
	if(lineNumber != 8)
	{
		errorid = ERROR_INCORRECT_FILE;
		return -1;
	}
	
	if (*r < 0 || *a < 0 || *b < 0 || *m < 0 || *k < 0 || *l < 0 || *dt < 0)
	{
		errorid = ERROR_INVALID_VALUES;
		return -1;
	}
	
	return 0;
}

/*
 * Reads ascii map file
 */
MapCell** readMap(char *mapFileName, int *nx, int *ny)
{
	int i, j;
	char *tempValue;
	char *numberOfRows, *numberOfColumns;
	char line[NMAX * 2 + 2];
	FILE *filePointer;
	MapCell** map;
	
	filePointer = fopen(mapFileName, "r");
	if(!filePointer)
	{
		errorid = ERROR_OPENING_FILE;
		return NULL;
	}
	
	/*
	 * Read the map dimensions
	 */
	fgets(line, sizeof(line), filePointer);
	numberOfColumns = strtok(line, " ");
	numberOfRows = strtok(NULL, "\n");
	
	*nx = atoi(numberOfRows);
	*ny = atoi(numberOfColumns);
	
	/*
	 * Check if dimensions are within limits
	 */
	if((*nx > NMAX) || (*ny > NMAX))
	{
		errorid = ERROR_INVALID_VALUES;
		return NULL;
	}
	else if((*nx < 1) || (*ny < 1))
	{
		errorid = ERROR_INVALID_VALUES;
		return NULL;
	}
	
	/*
	 * Increase each dimension by 2, to allocate some extra space for the Halos
	 */
	*nx = *nx + 2;
	*ny = *ny + 2;
	
	/*
	 * Allocate space for map array (arralloc is used, instead of malloc)
	 */
	map = (MapCell**)arralloc(sizeof(MapCell), 2, *nx, *ny);
	
	/*
	 * Read the whole file and store the map in a 2 dimensional array
	 */
	i = 1;
	while(fgets(line, sizeof(line), filePointer) != NULL)
	{
		tempValue = strtok(line, " ");
		map[i][1].bit = atoi(tempValue);
		
		for(j = 2; j < (*ny - 1); j++)
		{
			tempValue = strtok(NULL, " ");
			map[i][j].bit = atoi(tempValue);
		}
		i++;
	}
	
	fclose(filePointer);
	
	return map;
}








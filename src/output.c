#include <stdio.h>
#include <stdlib.h>

#include "../inc/readInput.h"
#include "../inc/output.h"
#include "../inc/computations.h"
#include "../inc/error.h"

/*
* Does a linear map from double value to a pixel value.
* The output is clamped to stay inside the defined min and max.
*/
int mapPixel(double valMin, double valMax, double val, int pixMin, int pixMax)
{
	if (valMax < valMin || pixMax < pixMin)
	{
		errorid = ERROR_MAP_PIXEL;
		return -1;
	}

	int out = (int) ((val - valMin) * (double)(pixMax - pixMin) / (valMax - valMin)) + pixMin;

	if (out < pixMin) return pixMin;
	else if (out > pixMax) return pixMax;
	else return out;
} 


/*
* Writes out a PPM format picture representing the current map.
* 
* land  - black
* water - light gray
* pumas - red (according to density)
* hares - green (according to density) 
*/
int writePPM(int nx, int ny, MapCell **map, double **hareDen, double **pumaDen, char *filename)
{
	int i, j;
	double minP, minH, maxP, maxH;
	
	getMaxMins(nx, ny, hareDen, pumaDen, &minP, &minH, &maxP, &maxH);

	FILE *f = fopen(filename, "w");
	if (!f)
	{
		errorid = ERROR_WRITE_PPM;
		return -1;
	}

	/* PPM header */
	fprintf(f, "%s %d %d %d\n", PPMTYPE, ny - 2, nx - 2, MAXCOLOR);

	for (i = 1; i < nx - 1; i++)
	{
		for (j = 1; j < ny - 1; j++)		
		{
			if (map[i][j].bit == 0)
			{
				fprintf(f, "%s  ", LIGHTGRAY);
			} 
			else
			{ 
				fprintf(f, "%d %d 0  ", mapPixel(minP, maxP, pumaDen[i][j], 0, MAXCOLOR),
				mapPixel(minH, maxH, hareDen[i][j], 0, MAXCOLOR));      
			} 
		}
		fprintf(f, "\n");
	}

	fclose(f);
	return 0;
}

/*
 * Print user settable parameters
 */
void printParameters(double r, double a, double b, double m, double k, double l, double dt)
{
	printf("r = %lf\n", r);
	printf("a = %lf\n", a);
	printf("b = %lf\n", b);
	printf("m = %lf\n", m);
	printf("k = %lf\n", k);
	printf("l = %lf\n", l);
	printf("dt = %lf\n", dt);
}

/*
 * Print densities of land cells
 */
void printDensities(int nx, int ny, MapCell **map, double **oldHareDen, double **oldPumaDen)
{
	int i, j;
	
	for(i = 1; i < (nx - 1); i++)
	{
		for(j = 1; j < (ny - 1); j++)
		{
			if(map[i][j].bit == 1)
			{
				printf("Cell %d, %d\n", i, j);
				printf("Hare density: %lf\n", oldHareDen[i][j]);
				printf("Puma density: %lf\n", oldPumaDen[i][j]);
			}
		}
	}
}

/*
 * Print map (1 for land, 0 for water)
 */
void printMap(int nx, int ny, MapCell **map)
{
	int i, j;
	
	printf("%d %d\n", ny-2, nx-2);
	
	for(i = 1; i < (nx - 1); i++)
	{
		for(j = 1; j < (ny - 1); j++)
		{
			printf("%d ", map[i][j].bit);
		}
		printf("\n");
	}
}

/*
 * Print the usage message for main
 */
void printUsage(void)
{
	fprintf(stderr, "\nThe arguments you can provide are:\n"
			"\t-f <name of map file> (Required)\n"
			"\t-T <Output frequency (default = 125)>\n"
			"\t-o to enable output files\n\n"
			"Type './main -h' for this message\n\n");
}

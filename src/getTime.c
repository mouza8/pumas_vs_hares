#include <sys/time.h>
#include "../inc/getTime.h"

/*
 * Returns time elapsed between two timevals.
 */
double timeDifference(struct timeval *endTime, struct timeval *startTime)
{
	double timeDifference;

	timeDifference = (endTime->tv_sec * 1000000 + endTime->tv_usec) - (startTime->tv_sec * 1000000 + startTime->tv_usec);

	return (timeDifference / 1000000.0);
}
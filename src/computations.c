#include "../inc/readInput.h"
#include "../inc/computations.h"
#include "../inc/error.h"

/*
 * Insert halos equal to 0 into map
 */
int insertHalos(int nx, int ny, MapCell **map)
{
	int i, j;
	
	if (!map)
	{
		errorid = ERROR_NULL_POINTER;
		return -1;
	}
	
	for(j = 0; j < ny; j++)
	{
		map[0][j].bit = 0;
		map[nx-1][j].bit = 0;
	}
	
	for(i = 0; i < nx; i++)
	{
		map[i][0].bit = 0;
		map[i][ny-1].bit = 0;
	}
	
	return 0;
}

/*
 * Initialize map densities
 */
int initializeMapDensities(int nx, int ny, MapCell **map, double **oldHareDen, double **oldPumaDen, double **hareDen, double **pumaDen)
{
	int i, j;
	double randomNumber;
	
	if (!map || !oldPumaDen || !oldHareDen || !hareDen || !pumaDen)
	{
		errorid = ERROR_NULL_POINTER;
		return -1;
	}
	
	for(i = 1; i < (nx - 1); i++)
	{
		for(j = 1; j < (ny - 1); j++)
		{
			randomNumber = (double)(rand() % 6) * map[i][j].bit;
			oldHareDen[i][j] = randomNumber;
			hareDen[i][j] = randomNumber;
			randomNumber = (double)(rand() % 6) * map[i][j].bit;
			oldPumaDen[i][j] = randomNumber;
			pumaDen[i][j] = randomNumber;
		}
	}
	
	return 0;
}

/*
 * Calculate number of land neighbours for each cell
 */
int calculateLandNeighbours(int nx, int ny, MapCell **map)
{
	int i, j;
	
	if (!map)
	{
		errorid = ERROR_NULL_POINTER;
		return -1;
	}
	
	for(i = 1; i < (nx - 1); i++)
	{
		for(j = 1; j < (ny - 1); j++)
		{
			map[i][j].numberOfLandNeighbours = (map[i][j-1].bit + map[i][j+1].bit + map[i-1][j].bit + map[i+1][j].bit) * map[i][j].bit;
		}
	}
	
	return 0;
}

/*
 * Calculates the total land cells in the map
 */
int calculateLandCells(int nx, int ny, MapCell **map)
{
	int i, j;
	int land = 0;
	
	if (!map)
	{
		errorid = ERROR_NULL_POINTER;
		return -1;
	}
	
	for(i = 1; i < (nx - 1); i++)
	{
		for(j = 1; j < (ny - 1); j++)
		{
			if (map[i][j].bit == 1)
				land++;
		}
	}
	
	return land;
}

/*
* Calculates the average density of pumas and hares.
*/
int getAvges(int nx, int ny, int land, double **hareDen, double **pumaDen, double *avgP, double *avgH) 
{
	double sumP = 0.0, sumH = 0.0;
	int i, j;
  
	if (!hareDen || !pumaDen || !avgP || !avgH)
	{
		errorid = ERROR_NULL_POINTER;
		return -1;
	}
  
	for (i = 1; i < nx-1; i++)
	{
		for (j = 1; j < ny-1; j++)
		{
			sumP += pumaDen[i][j];
			sumH += hareDen[i][j];
		}
	}
	*avgP = sumP / land;
	*avgH = sumH / land;
	
	return 0;
}


/*
* Returns the smaller of the two values.
*/
double dmin(double a, double b)
{
  return (a > b) ? b : a;
}

/*
* Returns the larger of the two values.
*/
double dmax(double a, double b)
{
  return (a > b) ? a : b;
}


/*
* Calculates the min and max density of pumas and hares.
*/
int getMaxMins(int nx, int ny, double **hareDen, double **pumaDen, double *minP, double *minH, double *maxP, double *maxH) 
{
	if (!hareDen || !pumaDen || !minP || !minH || !maxP || !maxH)
	{
		errorid = ERROR_NULL_POINTER;
		return -1;
	}
	
	*minP = pumaDen[0][0], *minH = hareDen[0][0], *maxP = pumaDen[0][0], *maxH = hareDen[0][0];
	int i, j;
  
	for (i = 1; i < nx - 1; i++)
	{
		for (j = 1; j < ny - 1; j++)
		{
			*minP = dmin(*minP, pumaDen[i][j]);
			*minH = dmin(*minH, hareDen[i][j]);
			*maxP = dmax(*maxP, pumaDen[i][j]);
			*maxH = dmax(*maxH, hareDen[i][j]);
		}
	}
	
	return 0;
}
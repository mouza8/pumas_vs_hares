#include "../inc/readInput.h"
#include "../inc/updateValues.h"
#include "../inc/error.h"

/*
 * Calculate the new densities of both animals
 */
int calculateNewAnimalDensities(double oldHare, double oldHareUp, double oldHareDown, double oldHareLeft, double oldHareRight, double oldPuma, double oldPumaUp, double oldPumaDown, double oldPumaLeft, double oldPumaRight, int landNeighbours, double r, double a, double k, double b, double m, double l, double dt, double *hareDensity, double *pumaDensity)  
{
	double haresBorn,haresKilled,hareMigration;
	double pumasBorn,pumasDied,pumaMigration;
	
	if (!hareDensity || !pumaDensity)
	{
		errorid = ERROR_NULL_POINTER;
		return -1;
	}
	
	haresBorn = r * oldHare;
	haresKilled = a * oldHare * oldPuma;
	hareMigration = k * ((oldHareUp + oldHareDown + oldHareLeft + oldHareRight) - (landNeighbours * oldHare)); 

	pumasBorn = b * oldPuma * oldHare;
	pumasDied = m * oldPuma;
	pumaMigration = l * ((oldPumaUp + oldPumaDown + oldPumaLeft + oldPumaRight) - (landNeighbours * oldPuma));

	*hareDensity = oldHare + dt * (haresBorn - haresKilled + hareMigration);
	*pumaDensity = oldPuma + dt * (pumasBorn - pumasDied + pumaMigration);
	
	return 0;
}
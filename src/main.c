#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <getopt.h>

#include "../inc/readInput.h"
#include "../inc/computations.h"
#include "../inc/updateValues.h"
#include "../inc/output.h"
#include "../inc/getTime.h"
#include "../inc/arralloc.h"

int allocArrays(double ***oldHareDen, double ***hareDen, double ***oldPumaDen, double ***pumaDen, int nx, int ny)
{
	*oldHareDen = (double**)arralloc(sizeof(double), 2, nx, ny);
	if (!*oldHareDen){
		return -1;
	}
	
	*oldPumaDen = (double**)arralloc(sizeof(double), 2, nx, ny);
	if (!*oldPumaDen){
		free(*oldHareDen);
		return -1;
	}
	
	*hareDen = (double**)arralloc(sizeof(double), 2, nx, ny);
	if (!*hareDen){
		free(*oldHareDen);
		free(*oldPumaDen);
		return -1;
	}
	
	*pumaDen = (double**)arralloc(sizeof(double), 2, nx, ny);
	if (!*pumaDen){
		free(*oldHareDen);
		free(*oldPumaDen);
		free(*hareDen);
		return -1;
	}
	
	return 0;
}

void swapArrays(double ***oldHareDen, double ***oldPumaDen, double ***hareDen, double ***pumaDen)
{
	double **temphare = *hareDen;
	double **temppuma = *pumaDen;
	
	*hareDen = *oldHareDen;
	*pumaDen = *oldPumaDen;
	
	*oldHareDen = temphare;
	*oldPumaDen = temppuma;
}

int main(int argc, char* argv[])
{
	int i, j, iter;
	char *mapFileName = NULL;
	int nx, ny;								/* Map dimensions */
	int land;								/* Number of land cells */
	double r, a, b, m, k, l, dt;			/* User defined parameters */
	double t = 0, tMax = 500;
	MapCell** map;							/* Map array */
	double avgH, avgP;						/* Average Densities */
	char buf[24]; 							/*Output filename buffer */
	struct timeval startTime, endTime;
	double totalTime;
	double **oldHareDen, **oldPumaDen;		/* Arrays of old dencities */
	double **hareDen, **pumaDen;			/* Arrays of new densities */
	int c;
  	int T = 125;		 					/* Output frequency */
	int outputEnabled = 0;

	
	gettimeofday(&startTime, NULL);
	
	/*
	 * Read command line arguments
	 */	
	while ((c = getopt(argc, argv, "f:T:ho")) != -1)
		switch (c)
		{
			case 'f':
				mapFileName = strdup(optarg);
				break;
			case 'T':
				T = atoi(optarg);
				break;
			case 'o':
				outputEnabled = 1;
				break;
			default:
				printf("Unknown option -%c\n", c);
			case 'h':
				printUsage();
				exit(-1);
				break;
		}
		
	if (!mapFileName)
	{
		printUsage();
		exit(-1);
	}
	
	/*
	 * Read parameters.txt
	 */
	if (readParameters("dat/parameters.txt", &r, &a, &b, &m, &k, &l, &dt))
	{
		printError("readParameters:");
		free(mapFileName);
		exit(-1);
	}
	printf("Parameters have been read successfully\n");

	/*
	 * Read map and arrays allocation
	 */
	map = readMap(mapFileName, &nx, &ny);
	if (!map)
	{
		printError("readMap:");
		free(mapFileName);
		exit(-1);
	}
	printf("Map allocated and read successfully\n");	
	
	free(mapFileName);
	
	if (allocArrays(&oldHareDen, &hareDen, &oldPumaDen, &pumaDen, nx, ny))
	{
		printf("Not enough memory...Exiting\n");
		free(map);
		exit(-1);
	}
	printf("Arrays allocated successfully\n");	
	
	/*
	 * Insert Halos
	 */
	if (insertHalos(nx, ny, map))
	{
		printError("insertHalos:");
		free(map);
		free(oldHareDen);
		free(oldPumaDen);
		free(hareDen);
		free(pumaDen);
		exit(-1);
	}
		
	/*
	 * Initialize hare and puma density in each map land cell
	 */
	if (initializeMapDensities(nx, ny, map, oldHareDen, oldPumaDen, hareDen, pumaDen))
	{
		printError("initializeMapDensities:");
		free(map);
		free(oldHareDen);
		free(oldPumaDen);
		free(hareDen);
		free(pumaDen);
		exit(-1);
	}
		
	/*
	 * Calculate number of land-neighbours for each cell
	 */
	if (calculateLandNeighbours(nx, ny, map))
	{
		printError("calculateLandNeighbours:");
		free(map);
		free(oldHareDen);
		free(oldPumaDen);
		free(hareDen);
		free(pumaDen);
		exit(-1);
	}
	
	/*
	 * Calculate number of land cells
	 */
	land = calculateLandCells(nx, ny, map);
	if (land < 0)
	{
		printError("calculateLandNeighbours:");
		free(map);
		free(oldHareDen);
		free(oldPumaDen);
		free(hareDen);
		free(pumaDen);
		exit(-1);
	}
		
	/*
	 * Calculate and update animal densities over timesteps
	 */
	for(iter = 0 ; t<tMax ; iter++)
	{
		for(i = 1; i < nx-1; i++)
		{
			for(j = 1; j < ny-1; j++)
			{
				if (map[i][j].bit == 1)
				{
					if (calculateNewAnimalDensities(oldHareDen[i][j], oldHareDen[i-1][j],
												oldHareDen[i+1][j], oldHareDen[i][j-1],
												oldHareDen[i][j+1], oldPumaDen[i][j],
												oldPumaDen[i-1][j], oldPumaDen[i+1][j],
												oldPumaDen[i][j-1], oldPumaDen[i][j+1],
												map[i][j].numberOfLandNeighbours, r, a, k, b, m, l, dt,
												&hareDen[i][j], &pumaDen[i][j]))
					{
						printError("calculateNewAnimalDensities:");
						free(map);
						free(oldHareDen);
						free(oldPumaDen);
						free(hareDen);
						free(pumaDen);
						exit(-1);
					}
				}
			}
		}
		if ( (iter % T == 0 || iter == tMax/dt) && outputEnabled)
		{
			/*
			* Print average densities.
			*/
			if (getAvges(nx, ny, land, hareDen, pumaDen, &avgP, &avgH))
			{
				printError("getAvges:");
				free(map);
				free(oldHareDen);
				free(oldPumaDen);
				free(hareDen);
				free(pumaDen);
				exit(-1);
			}
			printf("Iteration: %d Average H: %f, average P: %f\n", iter, avgH, avgP);
	 
			/*
			* Write PPM image of map.
			*/
			sprintf(buf, "out/iter%d.ppm", iter);
			if (writePPM(nx, ny, map, hareDen, pumaDen, buf))
			{
				printError("writePPM:");
				free(map);
				free(oldHareDen);
				free(oldPumaDen);
				free(hareDen);
				free(pumaDen);
				exit(-1);
			}
		}
      
		t += dt;
		swapArrays(&oldHareDen, &oldPumaDen, &hareDen, &pumaDen);
	}
	
	gettimeofday(&endTime, NULL);
	totalTime = timeDifference(&endTime, &startTime);
	printf("Total execution time is %.4lf seconds\n", totalTime);
	
	/*
	 * Memory de-allocation
	 */
	free(map);
	free(oldHareDen);
	free(oldPumaDen);
	free(hareDen);
	free(pumaDen);
	
	return 0;
}





	

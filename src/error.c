#include <stdio.h>
#include "../inc/error.h"

/*
 * Prints an error according to the value of the global variable errorid
 */
void printError(const char *mesg)
{
	switch (errorid)
	{
		case ERROR_OPENING_FILE:
			printf("%s: No such file or directory\n", mesg);
			break;
		case ERROR_INCORRECT_FILE:
			printf("%s: Incorrect file format\n", mesg);
			break;
		case ERROR_INVALID_VALUES:
			printf("%s: Invalid values in input file\n", mesg);
			break;
		case ERROR_NULL_POINTER:
			printf("%s: Null poonter passed to function\n", mesg);
			break;
		case ERROR_MAP_PIXEL:
			printf("%s: Wrong input to pixel mapping function.\n", mesg);
			break;
		case ERROR_WRITE_PPM:
			printf("%s: Error opening output file. (Probably invalid name)\n", mesg);
			break;
		case SUCCESS:
			printf("%s: Success\n", mesg);
			break;
		default:
			printf("%s: Unknown error\n", mesg);
			break;
	}	
}